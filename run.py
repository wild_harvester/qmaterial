# -*- coding:utf-8 -*-

__author__ = 'cjay'

import os
import sys

try:
    import ctypes

    myappid = 'kb23.qmaterial.kitchensink'
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
except Exception:
    pass

from PyQt5 import QtGui
from kb23.material_ui.app import MaterialApp

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.abspath(__file__))
    qml_dir = os.path.join(dir_path, "qmls")

    app = QtGui.QGuiApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon("icon.png"))

    window = MaterialApp(app, "MaterialUI Kitchensink", qml_dir)
    window.setSource("main.qml")
    window.show()

    sys.exit(window.exit_code())
