import QtQuick 2.0
import QtQuick.Controls 1.3
import "std"
import "material"

Item{
	id: root
    anchors.fill: parent
    property string source
    property bool is_modified : false
    property string file_name: "Без имени"
    property string old_source: ""   

	Item{
		id: inner
		anchors.leftMargin: 16
		anchors.rightMargin: 4	
		anchors.bottom: actions.top
		anchors.top: parent.top
		anchors.left: parent.left
		anchors.right: parent.right


		Title{
			x: 16
			y: 30			

			text: file_name
			opacity: 0.7
		}

		Divider{
			id: toolbar
			width: root.parent.width
			y: 72
			x: -16
		}
		
		Item{
			anchors.left: parent.left
			anchors.top: toolbar.bottom
			anchors.right: parent.right
			anchors.bottom: parent.bottom
			
			anchors.topMargin: 8
			anchors.bottomMargin: 8

			TextArea{	
				id: text_area			
				objectName: "code_area"
				anchors.fill: parent

				anchors.leftMargin: 16
				anchors.rightMargin: 16
				
				verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
				font { pixelSize: 14 ; family: "Roboto"  }
				
				textColor: "#1a1a1a"
				backgroundVisible: false
				tabChangesFocus : true
				frameVisible: false
				text: source

				onCursorPositionChanged: {
					dispatcher.cursor_moved(cursorPosition)				
				}

				onTextChanged: {
					if (old_source != text){
						dispatcher.code_changed(text)
						source = text
						old_source = text					
					}				
				}
				Keys.onPressed: {
					// alt+enter
					if (
						(event.modifiers == Qt.AltModifier) && 
						((event.key == Qt.Key_Enter) || (event.key == 16777220))
					){					
							if(event.isAutoRepeat) return					
							dispatcher.show_info(text_area.cursorPosition)
							event.accepted = true;
					}

					// ctrl+q
					if ((event.modifiers == Qt.ControlModifier) && (event.key == 81)){					
						if(event.isAutoRepeat) return						
						dispatcher.toggle_noun(text_area.selectionStart, text_area.selectionEnd);
						event.accepted = true;
					}
					// cttl+w
					if ((event.modifiers == Qt.ControlModifier) && (event.key == 87)){
						if(event.isAutoRepeat) return
						dispatcher.toggle_precedent(text_area.selectionStart, text_area.selectionEnd)
						event.accepted = true;
					}
					// ctrl+e
					if ((event.modifiers == Qt.ControlModifier) && (event.key == 69)){
						if(event.isAutoRepeat) return
						dispatcher.toggle_package(text_area.selectionStart, text_area.selectionEnd)
						event.accepted = true;
					}
					// ctrl+s
					if ((event.modifiers == Qt.ControlModifier) && (event.key == 83)){
						if(event.isAutoRepeat) return
						dispatcher.save_current_file();
						event.accepted = true;
					}		        
			    }

			}

			ScrollBar{
				target: text_area.flickableItem		
				// offset_y: text_area.y
			}

		}
		
		
	}
	Item{
		id: actions
		width: root.width
		height: 52
		anchors.bottom: root.bottom		

		Item{
			id: buttons
			anchors.fill: parent
			anchors.topMargin: 8
			anchors.bottomMargin: 8
			anchors.rightMargin: 4
			anchors.leftMargin: 4
			
			FlatButton{
				anchors.right: parent.right				
				// colored: true
				height:36
				text: "сохранить"
				disabled: !is_modified
				onClicked: dispatcher.save_current_file()
			}
		}
	}
	Component.onCompleted: {
    	old_source = source
    }

}