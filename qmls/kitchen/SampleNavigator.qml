import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 500		
	height: 400

	IconButton{
			y:20
	        path: "icons/database.svg"                
	        onClicked: nav.showed = true
    }
	
	Item{		
		width: 270
		height: 400
		clip: true
		// anchors.verticalCenter: parent.verticalCenter
		// anchors.horizontalCenter: parent.horizontalCenter

		PersistentNavigator{
			id: nav
			content: Column{
				width: parent.width
				ListItem{title: "Hello1"}
				ListItem{title: "Hello2"; active: true}
				ListItem{title: "Hello3"}
				ListItem{title: "Hello4"; secondary: IconButton{}}
				ListItem{title: "Hello5"}
				ListItem{title: "Hello6"}
			}
		}
		
	}
	

}