import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 800		
	height: 800
	// anchors.fill: parent


	Item{
		y: 120
		height: 400
		width: root.width 
		
		Dialog{
			title: "Use Google`s location service?"
			width: 56*5
			// height: 100
			content: Text{ 
				width: parent.width
				opacity: 0.54
				wrapMode: Text.Wrap
				text: "Let Google help apps datermine location. This means sending anonymous location data to Google, even when no apps are running."
			}
			header: Tabs{
				content:[
					TabItem{ title: "Tab1"; active: true; onClicked:{}},
					TabItem{ title: "Tab2"; onClicked:{}},
					TabItem{ title: "Tab3"; onClicked:{}}
				]
			}

			actions: Row{                            
		        FlatButton{
		        	color: p1_500
		            text: "DISAGREE"
		        }
		        FlatButton{
		        	color: p1_500
		            text: "AGREE"
		        }
		        
		    }
		}	
	}


}