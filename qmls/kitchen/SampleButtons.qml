import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 600		

	Rectangle{
        color: p_100
        width: 200
        height: 550
    }
    Rectangle{
        color: p_800
        width: 200
        height: 550
        x: 200
    }

    Item{
    	x: 20
    	y: 20
	Column{
		width: parent.width
		
		Item{
		    height: 90
		    width: root.width
		    RaisedButton{

		    }
		    RaisedButton{
		    	x: 90
		        disabled: true
		    }
		    RaisedButton{
		        x: 190
		        dark: true
		    }
		    RaisedButton{
		        x: 280
		        disabled : true
		        dark: true
		    }
		    // message
		    CodeSample{
		    	x: 400
		    	text: "RaisedButton{}"
		    }
		    CodeSample{
		    	dark: true
		    	x: 560
		    	text: "RaisedButton{\n    dark: true\n }"
		    }
		}
		Divider{ width: root.width}
		Item{width: 1; height: 8}		
		
		Item{
		    height: 90
		    width: root.width
		    FlatButton{

		    }
		    FlatButton{
		    	x: 90
		        disabled: true
		    }
		    FlatButton{
		        dark: true
		        x: 190
		    }
		    FlatButton{
		        dark: true
		        x: 280
		        disabled: true
		    }
		    // message
		    CodeSample{
		    	x: 400
		    	text: "FlatButton{}"
		    }
		    CodeSample{
		    	x: 560
		    	dark : true		    	
		    	text: "FlatButton{\n    dark: true\n}"
		    }
		}
		
		Divider{ width: root.width}
		Item{width: 1; height: 8}
		Item{
		    height: 110
		    width: root.width
		    FlatButton{
		        color: p1_500
		    }
		    FlatButton{
		    	disabled: true
		        x: 90
		        color: p1_500
		    }
		    FlatButton{
		        color: p1_500
		        x: 190
		        dark: true
		    }
		    FlatButton{
		        dark: true
		        x: 280
		        color: p1_500
		        disabled: true
		    }

		    // message
		    CodeSample{
		    	x: 400
		    	text: "FlatButton{\n    color: p1_500\n}"
		    }
		    CodeSample{
		    	dark: true
		    	x: 560
		    	text: "FlatButton{\n    dark: true\n    color: p1_500\n}"
		    }
		}
		Divider{ width: root.width}
		Item{width: 1; height: 8}
		Item{
		    height: 110
		    width: root.width
		    IconButton{

		    }
		    IconButton{
		    	x: 90
		        disabled: true
		    }
		    IconButton{
		        x: 190
		        color: p1_500
		        path: "icons/plus.svg"
		    }
		    IconButton{
		        x: 280
		        disabled: true
		        color: p1_500
		    }
		    // message
		    CodeSample{
		    	x: 400
		    	text: "IconButton{}"		    	
		    }
		    CodeSample{
		    	dark: true
		    	x: 560
		    	text: "IconButon{\n    dark: true\n}"
		    }
		}
		Divider{ width: root.width}
		Item{width: 1; height: 8}
    }
}
}