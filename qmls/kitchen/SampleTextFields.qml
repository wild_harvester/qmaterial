import QtQuick 2.0
import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 500		

	Row{
		spacing: 24
		Column{
			width: 320
			Text{				
				text: "Single-line text field"
			}
			Column{
				Row{
					Text{
						width: 160
						y: 13						
						opacity: 0.38
						text: "Normal with hint text"
					}
					TextField{
						width: 160
						value: ""
						hint: "Hint text"
					}		
				}
				Row{
					Text{
						width: 160
						text: "Hover"
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: ""
						hint: "Hint text"
					}		
				}
				Row{
					Text{
						width: 160
						text: "Press"
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: ""
						hint: "Hint text"
					}		
				}
				Row{
					Text{
						width: 160
						text: "Focus"
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: "Input tex"
						hint: "Hint text"
					}							
				}
				Row{
					Text{
						width: 160
						text: "Normal with input text"
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: "Input text"
						hint: "Hint text"
					}		
				}
				Row{
					Text{
						width: 160
						text: "Error"						
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: "Input text"
						hint: "Hint text"
						is_error: true
						error_message: "Username or Password is incorrect."
					}		
				}
				Row{
					Text{
						width: 160
						text: "Disabled"
						y: 13						
						opacity: 0.38
					}
					TextField{
						width: 160
						value: "Input text"
						disabled: true
						hint: "Hint text"
					}		
				}
			}
			
		}
		Column{
			width: 320
			Text{
				text: "Single-line text field with label"				
			}
			Column{
				Row{
					Text{
						width: 160
						y: 33						
						opacity: 0.38
						text: "Normal with hint text"
					}
					TextField{
						width: 160
						label: "Label text"
						hint: "Label hint"
					}		
				}
				Row{
					Text{
						width: 160
						y: 33						
						opacity: 0.38
						text: "Focus"
					}
					TextField{
						width: 160
						label: "Label text"
						hint: "Hint text"
						value: "Input text"
						icon: "icons/phone.svg"
					}		
				}
				Row{
					Text{
						width: 160
						y: 33						
						opacity: 0.38
						text: "Disabled"						
					}
					TextField{
						width: 160
						label: "Label text"
						hint: "Hint text"
						disabled: true
						value: "Input text"

					}		
				}
				Row{
					Text{
						width: 160
						y: 33						
						opacity: 0.38
						text: "Multi-line"						
					}
					TextField{
						width: 310
						label: "Description"
						hint: "Hint text"
						value: "Unique and rare dress from 1952. Made out of cotton with front pockets. Sleeveless with button closures."
						multiline: true

					}		
				}
			}			
		}

	}
	

}