import QtQuick 2.0

import "../material"
import "../editor_components"

Item{
	id: root
	width: 500		

	Item{
		x: 20
		width: 300
		height: 500

		List{
			content: 
			[
				ListItem{
					title: "Single-line item with secondary icon"
					secondary: [IconButton{}]
				},
				ListItem{
					title: "ListItem with image "
					image: "../imgs/photo_1.png"
				},
				ListItem{
					title: "List with main icon"
					icon: IconButton{}
				},
				ListItem{
					title: "ListItem with caption"
					description: "And description on second line"
				},
				ListItem{
					image: "../imgs/photo_1.png"
					title: "List with image and caption"
					description: "And description on second line"
				},
				ListItem{
					image: "../imgs/photo_1.png"
					title: "List with image and title"
					description: "And long description that tooks two lines."
				},
				ListItem{
					icon: IconButton{}
					title: "Title"
					description: "And long description that tooks two lines."
				},
				ListItem{
					title: "Title"
					icon: IconButton{}
					description: "Description between primary and secondary icons."
					secondary: IconButton{path: "../material/icons/information.svg"}
				}
			]
		}
	}
	


	

}