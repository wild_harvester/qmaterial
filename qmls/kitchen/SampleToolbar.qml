import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	anchors.fill: parent

	Toolbar{
		id: toolbar
		title: "My Title"
		subhead: "Subhead"
		button: IconButton{
			path: "icons/menu.svg"
			color: "white"			
		}
		tools: [
			IconButton{ color: "white"; path: "icons/magnify.svg"},
			IconButton{ color: "white"}
		]
		second_tools:[			
			IconButton{ color: "white"; path: "icons/close.svg"},
			IconButton{ color: "white"; path: "icons/database.svg"},
			IconButton{ color: "white"; path: "icons/file.svg"},
			IconButton{ color: "white"; path: "icons/hexagon.svg"}
		]
	}
	Item{
		width: parent.width
		anchors.top: toolbar.bottom
		height: 400
		List{
			content: 
			[
				ListItem{
					title: "Single-line item example very largeasdasdasdasd"
					secondary: [IconButton{}]
				},
				ListItem{
					title: "List with image asdasdadasdasdasdasd"
					image: "../imgs/photo_1.png"
				},
				ListItem{
					title: "List with image asdasdadasdasdasdasd"
					icon: IconButton{}
				},
				ListItem{
					title: "List with image asdasdadasdasdasdasd"
					description: "Blabla-bla our description be-be-be asd"
				},
				ListItem{
					image: "../imgs/photo_1.png"
					title: "List with image asdasdadasdasdasdasd"
					description: "Blabla-bla our description "
				},
				ListItem{
					image: "../imgs/photo_1.png"
					title: "List with image asdasdadasdasdasdasd"
					description: "Blabla-bla our description be-be-be asdasdasdasasdasdasd very long fuck ((((("
				},
				ListItem{
					title: "Title"
					icon: IconButton{}
					description: "Blabla-bla our description be-be-be asdasdasdasasdasdasd very long fuck ((((("
				},
				ListItem{
					title: "Title"
					icon: IconButton{}
					description: "Blabla-bla our description be-be-be asdasdasdasasdasdasd very long fuck ((((("
					secondary: IconButton{path: "../material/icons/information.svg"}
				}
			]
		}
	}
	

}