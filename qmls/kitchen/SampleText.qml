import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 800		
	height: 800
	// anchors.fill: parent

	Column{
		Row{
			Text{text: "Display 4"; width: 200}
			Display4{
			 text: "Light 112sp";
			 font.letterSpacing: (slider.x -100) / 100 * slider.ampl 
			}
		}
		Row{
			Text{text: "Display 3"; width: 200}
			Display3{text: "Regular 56sp"}
		}
		Row{
			Text{text: "Display 2"; width: 200}
			Display2{text: "Regular 45sp"}
		}
		Row{
			Text{text: "Display 1"; width: 200}
			Display1{text: "Regular 34sp"}
		}
		Row{
			Text{text: "Headline"; width: 200}
			Headline{text: "Regular 24sp"}
		}
		Row{
			Text{text: "Title"; width: 200}
			Title{text: "Medium 20sp"}
		}
		Row{
			Text{text: "Subhead"; width: 200}
			Subhead{text: "Regular 16sp"}
		}
		Row{
			Text{text: "Body 2"; width: 200}
			Body2{text: "Medium 14sp"}
		}
		Row{
			Text{text: "Body 1"; width: 200}
			Body1{text: "Regular 14sp"}
		}
		Row{
			Text{text: "Caption"; width: 200}
			Caption{text: "Regular 12sp"}
		}
		Row{
			Text{text: "Button"; width: 200}
			TextButton{text: "MEDIUM (ALL CAPS) 14sp"}
		}
		// font.letterSpacing: ((slider.x -100) / 100) * slider.ampl
		Item{
			width: 20
			height: 20
		}
		Rectangle{
			width: 200
			height: 10
			color: "gray"
			Text{
				y: 20
				text: (slider.x -100) / 100 * slider.ampl
			}
			Rectangle{
				property double ampl: 5
				id: slider
				color: "red"
				width: 20
				height: 20
				y: -5
				x: 100
			}
			MouseArea{
				anchors.fill: parent
				drag.target:  slider
				drag.axis: Drag.XAxis
				drag.minimumX: 0
				drag.maximumX: 200
			}
		}

	}
	

	


}