import QtQuick 2.0
import "../material"


Column{	
	id: root

	Rectangle{
		width: root.width
		height: 48
		color: is_selected ? type=="package" ? "#e29f2c" : type=="precedent" ? "#f92672" : p1_500 :  "white"

		Icon{
			x: 18 * level +6
			color: is_selected ? "white" : type=="package" ? "#e29f2c" : type=="precedent" ? "#f92672" : "black"
			path: is_highlighted ? "icons/hexagon.svg" : "icons/hexagon_outline.svg"
			anchors.verticalCenter: parent.verticalCenter
			opacity: 0.8
		}	    
		Text{
			x: 18 * (level + 1 ) + 18 + 2
			text: name
			color: is_selected ? "white" : is_highlighted ?  type=="package" ? "#e29f2c" : type=="precedent" ? "#f92672" : p1_500 :  "black"
			font.pixelSize: 13
			anchors.verticalCenter: parent.verticalCenter
			opacity: is_selected ? 1 : is_highlighted ? 1 : 0.7
		}			
		MouseArea{
			anchors.fill: parent
			onClicked: {				
				// is_open = ! is_open
				is_selected = true
				onItemSelected(root.parent)
			}

		}	
	}
	

	Column{
		visible: is_open
		Repeater{
			model: items
			objectName: "items_repeater"
			Loader{
				id: loader
				width: root.width
				source: model.modelData.is_leaf ? "LeafItem.qml" : "NodeItem.qml"

				property bool is_highlighted:  model.modelData.is_highlighted
				property int item_id:  model.modelData.item_id
				property string type:  model.modelData.type
				property var items:  model.modelData.items
				property string name:  model.modelData.name
				property bool is_leaf:  model.modelData.is_leaf
				property int level:  model.modelData.level
				// property string path:  model.modelData.path
				property bool is_open:  true
				property bool is_selected: false
				property var onSelect: root.onItemSelected

				onStatusChanged: {
	                if (loader.status == Loader.Ready) {   
                		if (opened_on_start){
							if (opened_on_start.indexOf(loader.path)==0){
								if (loader.is_dir){
								  loader.is_open = true   
								}else{
								  onSelect(loader)
								}

							}
							if (opened_on_start == loader.path){
                       			find_last_opened_on_start(loader)
                      		}
                			
                		}
	                }
         	     }
			}
		}
		
	}

}
