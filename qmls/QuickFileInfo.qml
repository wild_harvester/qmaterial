import QtQuick 2.1

Rectangle{
	id: root

    height: childrenRect.height
    width: 600
    color: "#3e4850"
    property string url
    property int pos

    signal clicked(string url, int pos)
    
    Rectangle{
        id: id_label
        color: "#353d44"                        
        height: 20
        width: parent.width
        anchors.top: parent.top

        Row{
        	spacing: 12
        	Text {
	            text: root.url
	            font.pointSize: 11                    
	            color: "#2d8b9f"                            
	            renderType: Text.NativeRendering
	        }
	        Text {
	            text: root.pos
	            font.pointSize: 11                    
	            color: "#2d8b9f"                            
	            renderType: Text.NativeRendering
	        }	
        }
        MouseArea{
        	anchors.fill: parent
        	onClicked: root.clicked(root.url, root.pos)
        }        
        
    }
}