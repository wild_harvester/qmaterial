import QtQuick 2.0


import "../std"
import "../material"


Item{              
    property string url                  
    property int pos
    property var background : p_100

    signal clicked()

    id: root
    
    height: txt_url.height + 4
    // height: 30    
    // height: 300

    Rectangle{
        y: 4
        width: txt_url.width 
        height: txt_url.height        
        color:  mouse.pressed ? Qt.darker(root.background, 1.4) : mouse.containsMouse? Qt.darker(root.background, 1.2) : root.background
        opacity: mouse.pressed ? 0.4 : mouse.containsMouse? 0.2 : 0

    }

    Caption{         
        y: 4                       
        color: p1_500
        id: txt_url
        width: root.width
        opacity: 0.7
        wrapMode: Text.Wrap
        text: url + " :" + pos
    }
    MouseArea {
        id: mouse
        anchors.fill: parent
        hoverEnabled: true        
        onClicked: root.clicked()
    }
}