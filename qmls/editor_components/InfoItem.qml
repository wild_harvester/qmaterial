import QtQuick 2.0


import "../std"
import "../material"


Item{              
    property string url                  
    property string content
    property int pos

    id: root
    
    height: txt_url.height + txt_node_content.contentHeight + 16
    // height: 300

    LinkItem{        
        id: txt_url
        url: root.url
        pos: root.pos
        width: root.width
        onClicked: dispatcher.open_file(url, pos)

    }
    Caption{           
        id: txt_node_content
        width: root.width
        opacity: 0.7
        wrapMode: Text.Wrap
        text: content
        y: txt_url.height + 4
        textFormat: Text.PlainText
    }
}