import QtQuick 2.0
import "tree"
import "std"
import "material"

Item{
	id: root
	anchors.fill: parent


	Tree{
		id: tree
		objectName: "structure_tree"
		anchors.fill: parent
	}	
	
}
