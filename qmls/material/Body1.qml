import QtQuick 2.0

Text{
	color: "black"
	font.family: "Roboto"
	opacity: 0.87
	font.pixelSize: 14
	font.letterSpacing: -0.25
	lineHeight: 20
	lineHeightMode: Text.FixedHeight
	textFormat: Text.PlainText
	property string type: "Body1"
}