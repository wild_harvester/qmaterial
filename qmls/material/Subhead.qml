import QtQuick 2.0

Text{
	color: "black"
	font.family: "Roboto"
	opacity: 0.87
	font.letterSpacing: 0
	font.pixelSize: 16
	lineHeight: 24
	lineHeightMode: Text.FixedHeight
	textFormat: Text.PlainText
	wrapMode: Text.Wrap
	property string type: "Subhead"
}

