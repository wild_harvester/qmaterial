import QtQuick 2.0

Text{
	color: "black"
	font.family: "Roboto"
	opacity: 0.87
	font.pixelSize: 24
	font.letterSpacing: -0.5
	lineHeight: 32
	lineHeightMode: Text.FixedHeight
	textFormat: Text.PlainText
	wrapMode: Text.Wrap
	property string type: "Headline"

}

