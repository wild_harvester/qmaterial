import QtQuick 2.0
import "../std"



FocusScope{
	id: root
    width: rect.width
    height: 32
    property alias title: txt_val.text
    property bool edit_mode: false
    property bool selected: false    
    property var selection_group
    property string icon: removed? "icons/undo-variant.svg" :"icons/close.svg"
    property bool inverted: false
    property bool removed: false


    opacity: removed? 0.5: 1

    signal clicked(var item)

    Rectangle{
    	color: "black"
		id: shadow                
        width: rect.width
        height: rect.height
        x: rect.x + 1
        y: rect.y + 1
        radius: 16
        opacity: mouse.containsMouse? 0.6: 0
        Behavior on opacity{ SmoothedAnimation{velocity: 3}}
    }
    
    Rectangle{
        id: rect
        width: txt_val.contentWidth + 12 + 8 + 32
        height: 32
        radius: 16
        color: removed? p2_100: inverted? p1_500 : p_300
        clip: true

        Rectangle{
        	visible: root.selected
        	color: removed? p2_500: inverted? p_300 : p1_500
        	width: parent.width
        	height: 32
        	radius: 16
        	x:0
        	y:0

        	Rectangle{
	        	color: inverted? p1_500 : p_300
	        	width: parent.width - 4
	        	height: 28
	        	radius: 14
	        	x:2
	        	y:2
	        }
        }


        Item{
        	id: round
        	anchors.right: parent.right
        	width: 32
        	height: 32
        	rotation: mouse.containsMouse || root.selected? 0 : -90
        	scale: mouse.containsMouse || root.selected? 1 : 0
        	
        	Behavior on rotation{ SmoothedAnimation{velocity: 260}}
        	Behavior on scale{ SmoothedAnimation{velocity: 3}}

        	MouseArea{
    			id: inner_mouse
    			anchors.fill: parent
    			hoverEnabled: true
    		}
        	
        	IconButton{
        		width: 32
        		height: 32
        		anchors.verticalCenter: parent.verticalCenter
        		path: root.icon
        		color: root.removed ? p2_400: p_800
        		onClicked: {
        			root.edit_mode = false
        			root.removed = !root.removed
        			// root.clicked(root)
        			if (root.removed == true){
        				selection_group.deselect_item(root)
        				root.selected = false
        			}else{
        				root.selected = true
        				select_item(Qt.ControlModifier)
        			}
        		}
        	}
        }

        Rectangle{
        	anchors.fill: txt_val
        	color: root.inverted ? p1_700 :p1_100
        	visible: root.edit_mode
        }

        TextEdit{
            id: txt_val
            text: "Без названия"
            font.pixelSize: 14
            readOnly : !root.edit_mode
            persistentSelection: true
            selectByMouse: true
            color: removed? selected? p2_400 : p2_200: inverted? "white" : "black"

            cursorDelegate: Rectangle{
            	color: txt_val.color
            	width: root.edit_mode? 1: 0
            	height: 14
            	opacity: 0.87
            }


            x: mouse.containsMouse || root.selected? 12 : 26
            y: 7
            // text: model.modelData
	        Behavior on x{ SmoothedAnimation{velocity: 80}}	   

	        Keys.onPressed: {
	        	if ((event.key == Qt.Key_Enter) || (event.key == 16777220)){
	        		edit_mode = false
	        	}
		    }
	        
        }    
    }    
      

    MouseArea{
    	id: mouse
    	anchors.fill: root
    	hoverEnabled: true
    	enabled: !root.edit_mode
    	acceptedButtons: Qt.NoButton
    }
    MouseArea{
    	id: mouse_text
    	width: root.width - 32
    	height: root.height
    	hoverEnabled: false
    	acceptedButtons: Qt.LeftButton
    	preventStealing: true
    	enabled: !root.edit_mode
    	onClicked:{    		
    		select_item(mouse.modifiers)
    	}
    	onDoubleClicked: {
    		root.edit_mode = true
    		txt_val.selectAll()
    		txt_val.focus = true
    	}
    }
    Component.onCompleted: {
    	if (selection_group){
    		selection_group.add_item(root)
    	}
    }
    Component.onDestruction: {
    	if (selection_group){
    		selection_group.remove_item(root)
    	}
    }


    onActiveFocusChanged:{
    	if (activeFocus==false){
    		root.edit_mode = false
    	}
    }



    function deselect_item(){
    	root.selected = false
		txt_val.deselect()
		root.edit_mode = false
    }
    function select_item(modifers){
		root.selected = true
		root.focus = true
		if (root.selection_group && !root.removed){
			root.selection_group.set_selection(root, modifers)
		}
    }

    
}