import QtQuick 2.0

Item{
	id: root
	property alias content: item_rect.children	
	property alias depth: shadow_rect.size
	// height: item_rect.height
	anchors.fill: parent
	z: depth

	Shadow{
		id: shadow_rect
		target: rect
		size: 2
	}
	Rectangle{
		id: rect
		width: root.width
		height: root.height

		color: "white"
		radius: 2

		Item{
			id: item_rect
			width: root.width
			height: root.height
			// height: 400
			// height: childrenRect.height > 60 ? childrenRect.height : 60
		}

	}	
}
