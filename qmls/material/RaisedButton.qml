import QtQuick 2.0
import "../std"

Item{
	id: root
	property bool dark: false
	property bool disabled: false
	property alias text: txt.text	
	signal clicked()

	clip: false

	width : rect.width + 8
	height: 32

	Shadow{
		id: shadow
        target: rect        
        visible: !disabled
        size: mouse.containsMouse? 8 : 2
        Behavior on size{ id: shadow_beh;SmoothedAnimation{velocity: 12}}
    }

	Rectangle{
		id: rect
		width: (txt.width + 16 ) < 64 ? 64 : txt.width + 16
		height: root.height
		color: dark ? disabled ? "white" :  mouse.pressed ? p1_700 : mouse.containsMouse? p1_600 : p1_500 : disabled ? p_300 :  mouse.pressed ? p_400 : mouse.containsMouse? p_300 : p_300
		radius: 2
		opacity: disabled ? 0.12 : 1

		anchors.verticalCenter: parent.verticalCenter
		anchors.horizontalCenter: parent.horizontalCenter

		clip: true

		Ripple{
			id: ripple
			color: txt.color
			y:  root.height/2
		}
		 		
	}
	TextButton{
		id: txt
		color: dark?  "white" : "black"
		text: "Button"
		anchors.verticalCenter: parent.verticalCenter
		anchors.horizontalCenter: parent.horizontalCenter
		opacity: dark ? disabled ? 0.3 : 0.9 : disabled ? 0.26 : 0.9
	}


	MouseArea {
		id: mouse
        anchors.fill: parent
        hoverEnabled: true
        enabled: !disabled
        onClicked: { 
        	// shadow.restart()
        	// shadow_beh.enabled = false
        	// shadow.size = 8
        	// shadow_beh.enabled = true
        	// shadow.size = 2

        	root.clicked()
        	ripple.x = mouse.x
        	ripple.anim()
        }
    }
}