import QtQuick 2.0


Text{
	id: txt
	color: "black"
	font.family: "Roboto"		
	font.pixelSize: 45	
	font.letterSpacing: -1	
	lineHeight: 48
	lineHeightMode: Text.FixedHeight
	textFormat: Text.PlainText
	wrapMode: Text.Wrap
	opacity: 0.54
	property string type: "Display2"
}	

