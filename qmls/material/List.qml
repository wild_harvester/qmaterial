import QtQuick 2.0

Item{
	id: root
	property alias content: content_rect.children	
	anchors.fill: parent

	Rectangle{
		anchors.fill: parent		
		border.width: 1
		border.color: "black"
		color: "transparent"
		opacity: 0.12
	}
	
	
	Item{
		anchors.fill: parent
		y: 8
		anchors.bottomMargin: 8
		anchors.topMargin: 8
		
		Flickable {    
	      id:  flick_panel
	      anchors.fill: parent
	      
	      contentWidth: parent.width
	      contentHeight: content_rect.height
	      clip: true

	      flickableDirection: Flickable.VerticalFlick
	      boundsBehavior: Flickable.StopAtBounds

	      Column{
				width: parent.width
				id: content_rect
			}

	    }
	}
	ScrollBar{
      target: flick_panel
    }
	

	


	
}
