import QtQuick 2.0

Text{
	color: "black"
	font.family: "Roboto Light"
	opacity: 0.54
	font.pixelSize: 112
	font.letterSpacing: -5
	lineHeight: 1.0
	lineHeightMode: Text.ProportionalHeight
	textFormat: Text.PlainText
	wrapMode: Text.Wrap
	property string type: "Display4"
}

