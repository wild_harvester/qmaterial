import QtQuick 2.0
import "../std"

Item{
	id: root
	// property alias content_height: item_rect.height
	// property alias content: item_rect.children	
	property alias actions: actions_row.children
	property alias description: desc_content.children	
	property string icon: ""
	property string title : ""
	property string subhead : ""
	property alias media: rct_media.children
	property string text: ""

	property bool show_details: false
	// anchors.fill: parent
	width: parent.width	
	height: clmn_cntnt.childrenRect.height

	Shadow{
		target: rect
		size: 2
	}
	Rectangle{
		id: rect
		width: root.width
		height: root.height

		color: p_100
		radius: 2
		clip: true


		Column{
			id: clmn_cntnt
			Rectangle{
				id: rct_media		
				width: root.width
				color: "red"
				height: childrenRect.height
			}
			Item{
				id: rct_title
				width: root.width
				height: root.icon.length>0? 72: 72+24
				visible: (root.title + root.icon + root.subhead).length> 0

				Image{			
					height: 40
					width: 40			
					y: 16
					x: 16
					source: root.icon
					visible: root.icon.length>0
				}

				Headline{
					text: root.title
					y: 24
					x: 16
					width: parent.width-16-16
					visible: root.icon.length<1
				}
				Body1{
					text: root.subhead
					x: 16
					y: root.icon.length>0? 0: 56
					width: parent.width-16-16
					visible: root.subhead.length>0
				}

				Body2{
					text: root.title
					y: 16
					x: 70
					width: parent.width-70-16
					visible: root.icon.length>0
				}
			}	
			Rectangle{
				id: rct_text
				width: root.width
				color: "lightgray"
				height: text_content.childrenRect.height + 16 + 24 
				visible: text.length>0
				Body1{
					x: 16
					y: 16
					width: parent.width-16-16
					id: text_content
					text: root.text
					wrapMode: Text.Wrap
				}
			}

			Rectangle{
				id: rct_actions
				width: root.width
				color: "white"
				height: actions_row.childrenRect.height + 16
				Row{			
					spacing: 8
					y: 8
					x: 8
					width: parent.width-16
					height: parent.height - 16
					id: actions_row
				}
				IconButton{
					y: 8
					x: parent.width-8-36
					path: root.show_details ? "icons/chevron-up.svg" : "icons/chevron-down.svg"
					visible: root.description.length>0

					onClicked: {
						root.show_details = !root.show_details
						if (root.show_details){
							rct_desc.height	= desc_content.childrenRect.height + 16 + 24 
						}else{
							rct_desc.height	= 0
						}
						
					}
				}
			}
			
			Rectangle{
				id: rct_desc
				width: root.width
				color: "lightgray"
				height: desc_content.childrenRect.height + 16 + 24 
				visible: root.show_details
				Item{
					x: 16
					y: 16
					width: parent.width-16-16
					id: desc_content
				}
			}
		}
	




	}	
}
