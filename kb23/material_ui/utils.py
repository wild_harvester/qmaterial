__author__ = 'cjay'

import re
pattern_word = re.compile(r'\w+')
pattern_word_end = re.compile(r'\w+$')
def pos2line(text, end):
    """
    Функция преобразует номер в тексте в позицию строка:буква
    :param text:
    :param end:
    :return:
    """
    count = 0
    pos = text.find("\n", 0, end + 1)
    lastline = 0
    while pos >= 0:
        count += 1
        lastline = pos
        pos = text.find("\n", pos + 1, end + 1)
    if lastline == 0:
        return 0, end
    if lastline == end:
        return count, 0
    return count, end - lastline - 1


def TestFindAstDiff(node1, node2):
    """
    ищем изменения одной структуры в другую
    :param node1: новая структура
    :param node2: старая структура
    :return:
    """
    removed = []
    # item =
    # for pos, item in enumerate(node1):
    # if node


def list_diff(l1, l2):
    """
    Данная функция ищет перестановки элементов в списках
    :param l1:
    :param l2:
    :return:
    """
    size = len(l1)
    if not size:
        return None
    left_index = 0
    right_index = 0
    missed_a = []

    while left_index < size:
        if l1[left_index] == l2[right_index]:
            right_index += 1
        else:
            missed_a.append(l1[left_index])
        left_index += 1
    tail = l2[right_index:]
    if tail and missed_a:
        rec_list = list_diff(missed_a, tail)
    return missed_a, tail


def get_word_at(textline, pos):
    """
    Возвращаем границы слова + флаг попадания в пробел
    :param textline:
    :param pos:
    :return:
    """

    assert pos >= 0, "Position must be positive number"
    assert pos < len(textline), "Pos must be lesser than text length"

    match_1 = pattern_word_end.search(textline[:pos])
    match_2 = pattern_word.match(textline, pos)

    text_length = len(textline)
    right_border = None
    left_border = None


    # Ничё не найдено
    if not match_1 and not match_2:
        return None

    if not match_1:
        if pos == 0:
            left_border = 0
        else:
            left_border = match_2.span()[0]

    if not match_2:
        return None

    if right_border == None:
        right_border = match_2.span()[-1]
    if left_border == None:
        left_border = match_1.span()[0]

    return (left_border, right_border)

def get_whitespaces(string, pos):
    i = pos
    length = len(string)
    while ((i+1)<(length)) and (string[i+1] == " "):
        i += 1
    return (pos, i+1)